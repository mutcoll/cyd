#include "dispatcher/Dispatcher.h"
#include "drawer.hpp"
#include "dummyGL.hpp"

int main(){
	Dispatcher dispatcher;
//	Manager_drawer drawer( dispatcher);
	Manager_dummyGL dummyGL( dispatcher);

	dispatcher.addEventHandler(
			[] (SDL_Event &event){ ((EventCallThis::wraper*) event.user.data1)->ff(); delete (EventCallThis::wraper*) event.user.data1;},
			[] (SDL_Event &event){ return EventCallThis::eventType == event.type;});
	dispatcher.addEventHandler( [&dispatcher] (SDL_Event &event){ dispatcher.endDispatcher();}, [] (SDL_Event &event){ return event.type == SDL_QUIT;});
	dispatcher.startDispatcher();

	exit(0);
	return 0;
}

