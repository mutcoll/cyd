#include "dispatcher/Dispatcher.h"
#include "dispatcher/Window.h"
#include "utils/EventCallThis.h"

class Manager_drawer: public Window{
public:
	Manager_drawer( Dispatcher &d);
	virtual ~Manager_drawer();
	void onDisplay( Uint32 ms);
private:
	Dispatcher &dispatcher;
	SDL_Renderer *renderer;
	SDL_Texture *texture;
	SDL_Surface *surface;
};

Manager_drawer::Manager_drawer( Dispatcher &d): dispatcher(d){
	this->renderer = SDL_CreateRenderer( this->window, -1, 0);
	this->texture = SDL_CreateTexture( this->renderer, SDL_PIXELFORMAT_ARGB8888, SDL_TEXTUREACCESS_STREAMING, 640, 480);
	this->surface = SDL_CreateRGBSurface(0,640,480,32,0,0,0,0);

	this->dispatcher.addTimerHandler( [this] (Uint32 ms){
			EventCallThis::pushEvent( [this,ms] (){ this->onDisplay( ms);});
		}, 30);
}

Manager_drawer::~Manager_drawer(){
	if( this->surface != nullptr){ SDL_FreeSurface( this->surface);};
	if( this->texture != nullptr){ SDL_DestroyTexture( this->texture);};
	if( this->renderer != nullptr){ SDL_DestroyRenderer( this->renderer);};
}

void Manager_drawer::onDisplay( Uint32 ms){
	// if( ms != 30); // ??

	SDL_Rect rect { 0, 0, this->surface->w, this->surface->h};
	SDL_FillRect( this->surface, &rect, SDL_MapRGB( this->surface->format, 0xff, 0x0, 0x0));
	rect.x = rect.w /= 10;
	rect.y = rect.h /= 10;
	rect.x *= SDL_GetTicks()/100  % 10;
	rect.y *= SDL_GetTicks()/1000 % 10;
	SDL_FillRect( this->surface, &rect, SDL_MapRGB( this->surface->format, 0x0, 0xff, 0x0));

	SDL_UpdateTexture( this->texture, nullptr, this->surface->pixels, this->surface->pitch);
	SDL_RenderClear( this->renderer);
	SDL_RenderCopy( this->renderer, this->texture, nullptr, nullptr);
	SDL_RenderPresent( this->renderer);
}

