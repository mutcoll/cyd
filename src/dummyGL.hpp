#include "dispatcher/Dispatcher.h"
#include "dispatcher/Window.h"
#include "graphics/drawables/Volador.h"

class Manager_dummyGL: public WindowGL{
public:
	Manager_dummyGL( Dispatcher &d);
	void onDisplay( Uint32 ms);
	bool isEvent( SDL_Event &event);
	void onEvent( SDL_Event &event);
//	void onKeyPressed( SDL_Event &event);
//	void onPressed( const Pressed &p);
//	void onMouseMotion( SDL_Event &e);
//	void onMouseButton( SDL_Event &e);
private:
	void drawAxes();
	Dispatcher &dispatcher;
	Volador vldr;
    Vector3D center;
};

Manager_dummyGL::Manager_dummyGL( Dispatcher &d): dispatcher(d){
	this->initGL();
	this->vldr.setPos( Vector3D(1, 2, 10));
	this->vldr.setOrientacion( this->center - vldr.getPos(), Vector3D(0, 1, 0));

	this->dispatcher.addTimerHandler( [this] (Uint32 ms){
			EventCallThis::pushEvent( [this,ms] (){ this->onDisplay( ms);});
		}, 30);
	this->dispatcher.addEventHandler( [this] (SDL_Event &event){ this->onEvent( event);}, [this] (SDL_Event &event){ return this->isEvent( event);});
}

void Manager_dummyGL::drawAxes() {
	glPushMatrix();
	glBegin(GL_LINES);
	glColor3f(1, 0, 0);
	glVertex3f(0, 0, 0);
	glVertex3f(100, 0, 0);

	glColor3f(0, 1, 0);
	glVertex3f(0, 0, 0);
	glVertex3f(0, 100, 0);

	glColor3f(0, 0, 1);
	glVertex3f(0, 0, 0);
	glVertex3f(0, 0, 100);
	glEnd();

	glPopMatrix();
}

void Manager_dummyGL::onDisplay( Uint32 ms) {
	// Clear The Screen And The Depth Buffer
	glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );

	glLoadIdentity();                // Reset The View
	this->vldr.Look();
	//	glTranslatef( 0, 0, -5.0);

	this->drawAxes();
	glPushMatrix();

	glBegin(GL_POLYGON);
	glColor4f(0, 0.5, 1, 0.2);
	glVertex3f(-1.0, 0.0, 0.0);
	glColor4f(0.5, 1, 0, 0.2);
	glVertex3f(1.0, 0.0, 0.0);
	glColor4f(1, 0, 0.5, 0.2);
	glVertex3f(0.0, 1.0, 0.0);
	glEnd();


	glPopMatrix();
	// swap buffers to display, since we're double buffered.
	SDL_GL_SwapWindow( this->window);
}

bool Manager_dummyGL::isEvent( SDL_Event &event){
	bool r = false;
	switch( event.type){
	case SDL_KEYDOWN:
	case SDL_KEYUP:
		if( event.key.windowID == SDL_GetWindowID( this->window)){ r = true;};
		break;
	case SDL_MOUSEMOTION:
		if( event.motion.windowID == SDL_GetWindowID( this->window)){ r = true;};
		break;
	case SDL_MOUSEBUTTONDOWN:
	case SDL_MOUSEBUTTONUP:
		if( event.button.windowID == SDL_GetWindowID( this->window)){ r = true;};
		break;
	}
	return r;
}

void Manager_dummyGL::onEvent( SDL_Event &event){
	switch( event.type){
	case SDL_KEYDOWN:
		break;
	case SDL_KEYUP:
		break;
	case SDL_MOUSEMOTION:
		break;
	case SDL_MOUSEBUTTONDOWN:
		break;
	case SDL_MOUSEBUTTONUP:
		break;
	}
}
/*
void Window::onPressed(const Pulsado &p) {
	//cout << "p.sym = " << p.sym << endl;	// DEPURACION
	if (p.sym == SDLK_u
		|| p.sym == SDLK_o
		|| p.sym == SDLK_i
		|| p.sym == SDLK_k
		|| p.sym == SDLK_j
		|| p.sym == SDLK_l
		|| p.sym == SDLK_w
		|| p.sym == SDLK_s
		|| p.sym == SDLK_a
		|| p.sym == SDLK_d
		|| p.sym == SDLK_q
		|| p.sym == SDLK_e) {
		//semaforoStep.cerrar();
	semaforoDisplay.sumar();
//        glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
	}

	switch(p.sym) {
		case SDLK_u:
			//vldr.setPos(vldr.getPos() + Vector3D(0, dtrl, 0));
			    vldr.rotatef(-drot, 0, 0, 1);
			break;
		case SDLK_o:
			//vldr.setPos(vldr.getPos() + Vector3D(0, -dtrl, 0));
			    vldr.rotatef(drot, 0, 0, 1);
			break;
		case SDLK_i:
			//vldr.setPos(vldr.getPos() + Vector3D(0, dtrl, 0));
			    vldr.rotatef(drot, 1, 0, 0);
			    center = vldr.getPos() + vldr.getDir()*(vldr.getPos()-center).Modulo();
			break;
		case SDLK_k:
			//vldr.setPos(vldr.getPos() + Vector3D(0, -dtrl, 0));
			    vldr.rotatef(-drot, 1, 0, 0);
			    center = vldr.getPos() + vldr.getDir()*(vldr.getPos()-center).Modulo();
			break;
		case SDLK_l:
			    vldr.rotY(-drot);
			    center = vldr.getPos() + vldr.getDir()*(vldr.getPos()-center).Modulo();
			break;
		case SDLK_j:
			    vldr.rotY(drot);
			    center = vldr.getPos() + vldr.getDir()*(vldr.getPos()-center).Modulo();
			break;
		case SDLK_e:
			center += vldr.getDir()*dtrl;
			vldr.setPos(vldr.getPos() + vldr.getDir()*dtrl);
			break;
		case SDLK_q:
			center -= vldr.getDir()*dtrl;
			vldr.setPos(vldr.getPos() - vldr.getDir()*dtrl);
			break;
		case SDLK_w:
			center += vldr.getUp()*dtrl;
			vldr.setPos(vldr.getPos() + vldr.getUp()*dtrl);
			break;
		case SDLK_s:
			center -= vldr.getUp()*dtrl;
			vldr.setPos(vldr.getPos() - vldr.getUp()*dtrl);
			break;
		case SDLK_d:
			center -= vldr.getX()*dtrl;
			vldr.setPos(vldr.getPos() - vldr.getX()*dtrl);
			break;
		case SDLK_a:
			center += vldr.getX()*dtrl;
			vldr.setPos(vldr.getPos() + vldr.getX()*dtrl);
			break;
		default:
			break;
	}
}

void Gestor::onKeyPressed(SDL_Event &e) {

    semaforoDisplay.sumar();
    if (e.type == SDL_KEYUP) {
        return;
    }

    switch(e.key.keysym.sym) {
        case SDLK_h:
            cout << help() << endl;
            break;
        case SDLK_SPACE:
            semaforoDisplay.sumar();
            semaforoStep.sumar();
            break;
        case SDLK_p:	// play / stop
            if (semaforoDisplay.estado())
            {
                semaforoStep.cerrar();
                semaforoDisplay.cerrar();
                semaforoDisplay.sumar();
            }
            else
            {
                semaforoStep.abrir();
                semaforoDisplay.abrir();
            }
            break;
        default:
            break;
    }
}

void Gestor::onMouseButton(SDL_Event &e)
{
    lastClickX = e.button.x;
    lastClickY = e.button.y;

    if (e.button.button == SDL_BUTTON_RIGHT) {
        rightClickPressed = e.button.type == SDL_MOUSEBUTTONDOWN;
    }

    if (e.button.button == SDL_BUTTON_LEFT) {
        leftClickPressed = e.button.type == SDL_MOUSEBUTTONDOWN;
    }

    if (e.button.type == SDL_MOUSEWHEEL) {
        vldr.setPos(vldr.getPos() + e.wheel.y*10*vldr.getDir()*dtrl);
    }
    semaforoDisplay.sumar();
}

void Gestor::onMouseMotion(SDL_Event &e)
{
    int w, h;
    SDL_GetWindowSize(window, &w, &h);

    if (leftClickPressed) {
        vldr.rotateAround(center, e.motion.yrel*mouse_drot, 1, 0, 0);
        vldr.externalRotateAround(center, -e.motion.xrel*mouse_drot, 0, 1, 0);
    }

    if (rightClickPressed) {
        Vector3D projection_y(vldr.getDir());
        float sense(projection_y.getY()<= 0? 1 : -1);
        projection_y.setY(0);
        Vector3D projection_x(vldr.getX());
        projection_x.setY(0);
        Vector3D translation(e.motion.yrel * mouse_dtrl * projection_y * sense + e.motion.xrel * mouse_dtrl * projection_x);
        center += translation;
        vldr.setPos(vldr.getPos() + translation);
    }
    semaforoDisplay.sumar();
}
*/


