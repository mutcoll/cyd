project(MyTemplateProject)
cmake_minimum_required(VERSION 2.8)

include(conanbuildinfo.cmake)
conan_basic_setup()


set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wall")
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11")
include_directories(src)
include_directories(include)

set(SOURCE_FILES
	#./src/Gestor.cpp
    )

set(EXECUTABLE_NAME cyd)


add_executable(${EXECUTABLE_NAME} src/main.cpp ${SOURCE_FILES})
target_link_libraries(${EXECUTABLE_NAME} ${CONAN_LIBS})

